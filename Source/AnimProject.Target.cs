// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class AnimProjectTarget : TargetRules
{
	public AnimProjectTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
        bOverrideBuildEnvironment = true;

        ExtraModuleNames.AddRange( new string[] { "AnimProject" } );
	}
}
