// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AnimProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ANIMPROJECT_API AAnimProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
