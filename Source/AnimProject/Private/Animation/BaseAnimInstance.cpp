// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseAnimInstance.h"
#include "BaseCharacter.h"
#include "Engine/Classes/Kismet/KismetMathLibrary.h"

void UBaseAnimInstance::NativeInitializeAnimation()
{
	PawnOwner = TryGetPawnOwner();
	if (PawnOwner) CharacterOwner = Cast<ABaseCharacter>(PawnOwner);
	if (CharacterOwner)
	{
		MovementComponent = CharacterOwner->GetMovementComponent();
		CharacterMovementComponent = CharacterOwner->GetCharacterMovement();
	}
}

void UBaseAnimInstance::NativeUpdateAnimation(float DeltaTime)
{
	if (CharacterOwner)
	{
		if (MovementComponent)
		{
			Speed = MovementComponent->Velocity.Size();
			bIsInAir = MovementComponent->IsFalling();
			Roll = UKismetMathLibrary::NormalizedDeltaRotator(PawnOwner->GetBaseAimRotation(), PawnOwner->GetActorRotation()).Roll;
			Pitch = UKismetMathLibrary::NormalizedDeltaRotator(PawnOwner->GetBaseAimRotation(), PawnOwner->GetActorRotation()).Pitch;
			Yaw = UKismetMathLibrary::NormalizedDeltaRotator(PawnOwner->GetBaseAimRotation(), PawnOwner->GetActorRotation()).Yaw;

			YawDelta =
				FMath::FInterpTo(YawDelta,
					UKismetMathLibrary::NormalizedDeltaRotator(RotationLastTick, PawnOwner->GetActorRotation()).Yaw / ((DeltaTime + 0.1f) / LeanIntensityScaling),
					DeltaTime,
					YawDeltaInterpSpeed);

			RotationLastTick = PawnOwner->GetActorRotation();
			bIsAccelerating = CharacterMovementComponent->GetCurrentAcceleration().Size() > 0;
			bFullBody = GetCurveValue("FullBody") > 0;
			MovementDirection = CharacterOwner->GetActorRotation().UnrotateVector(CharacterMovementComponent->Velocity);
		}
	}
}

//Overriden in child classes (PlayerAnimInstance & BossAnimInstance)
void UBaseAnimInstance::HandleMyNotify(FString Name)
{
	if (Name == "HitCheck")
	{
		CharacterOwner->HitCheck();
	}
	else if (Name == "ResetCombo")
	{
		CharacterOwner->ResetCombo();
	}
	else if (Name == "SaveAttack")
	{
		CharacterOwner->ComboAttackSave();
	}
	else if (Name == "JumpEnd")
	{
		CharacterOwner->OnJumpEnd();
	}
}

void UBaseAnimInstance::OnMyNotifyBegin(FString Name)
{
	if (Name == "TakingDamage")
	{
		CharacterOwner->OnDamageAnim(true);
	}
}

void UBaseAnimInstance::OnMyNotifyEnd(FString Name)
{
	if (Name == "TakingDamage")
	{
		CharacterOwner->OnDamageAnim(false);
	}
}
