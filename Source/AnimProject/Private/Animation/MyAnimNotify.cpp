// Fill out your copyright notice in the Description page of Project Settings.


#include "MyAnimNotify.h"
#include "BaseAnimInstance.h"

void UMyAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	UBaseAnimInstance * AnimInstance = Cast<UBaseAnimInstance>(MeshComp->GetAnimInstance());
	if (AnimInstance)
	{
		AnimInstance->HandleMyNotify(Name);
	}
}