// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerAnimInstance.h"
#include "MyPlayerCharacter.h"

void UPlayerAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();
	PlayerCharacter = Cast<AMyPlayerCharacter>(CharacterOwner);
}

void UPlayerAnimInstance::NativeUpdateAnimation(float DeltaTime)
{
	Super::NativeUpdateAnimation(DeltaTime);
}

void UPlayerAnimInstance::HandleMyNotify(FString Name)
{
	Super::HandleMyNotify(Name);
	
	if (Name == "DodgeEnd")
	{
		PlayerCharacter->OnDodgeEnd();
	}
}

void UPlayerAnimInstance::OnMyNotifyBegin(FString Name)
{
	Super::OnMyNotifyBegin(Name);

	if (Name == "Inv_Frame")
	{
		PlayerCharacter->OnDodgeInvincibilityStart();
	}
}

void UPlayerAnimInstance::OnMyNotifyEnd(FString Name)
{
	Super::OnMyNotifyEnd(Name);
	
	if (Name == "Inv_Frame")
	{
		PlayerCharacter->OnDodgeInvincibilityEnd();
	}
}
