// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"
#include "HealthComponent.h"
#include "Engine/Classes/Components/SkeletalMeshComponent.h"
#include "Engine/Classes/Kismet/KismetMathLibrary.h"
#include "..\..\Public\Characters\BaseCharacter.h"
#include "Player\MyPlayerCharacter.h"
#include "Kismet\GameplayStatics.h"
#include "BaseAIController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"

ABaseCharacter::ABaseCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(FName("HealthComponent"));
}

float ABaseCharacter::GetLookAtTargetYaw(AActor * Target) const
{

	return UKismetMathLibrary::Abs(UKismetMathLibrary::NormalizedDeltaRotator(UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Target->GetActorLocation()), GetActorRotation()).Yaw);
}

bool ABaseCharacter::IsTargetInMeleeRange(AActor * EnemyTarget) const
{
	return GetLookAtTargetYaw(EnemyTarget) < MeleeAngle && GetDistanceTo(EnemyTarget) < MeleeRadius;
}

void ABaseCharacter::OnJumpEnd()
{
	bIsJumping = false;
}

void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	MovementComponent = GetCharacterMovement();
	PlayerCharacter = Cast<AMyPlayerCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	AIController = Cast<ABaseAIController>(UAIBlueprintHelperLibrary::GetAIController(this));
}

void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

