// Copyright by LG7


#include "Behavior/BTServiceBase.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BaseCharacter.h"
#include "AIController.h"
#include "BaseAIController.h"

UBTServiceBase::UBTServiceBase()
{
	bNotifyBecomeRelevant = true;
}

void UBTServiceBase::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::OnBecomeRelevant(OwnerComp, NodeMemory);
	AIController = Cast<ABaseAIController>(OwnerComp.GetAIOwner());
	CharacterOwner = Cast<ABaseCharacter>(AIController->GetPawn());
}

