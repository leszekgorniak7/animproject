// Copyright by LG7


#include "Behavior/BTService_CheckMelee.h"
#include "BaseCharacter.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BaseAIController.h"

void UBTService_CheckMelee::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	OwnerComp.GetBlackboardComponent()->SetValueAsBool("IsEnemyInMeleeRange", AIController->CheckMelee(AIController->Target));
}

