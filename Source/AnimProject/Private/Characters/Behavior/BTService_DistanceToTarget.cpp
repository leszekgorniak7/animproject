// Copyright by LG7


#include "Behavior/BTService_DistanceToTarget.h"
#include "BaseCharacter.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BaseAIController.h"

void UBTService_DistanceToTarget::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	OwnerComp.GetBlackboardComponent()->SetValueAsFloat("DistanceToTarget", CharacterOwner->GetDistanceTo(AIController->Target));
}

