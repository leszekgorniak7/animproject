// Copyright by LG7


#include "Behavior/BTService_ShieldStatus.h"
#include "BaseCharacter.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BossCharacter.h"

void UBTService_ShieldStatus::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::OnBecomeRelevant(OwnerComp, NodeMemory);
	BossOwner = Cast<ABossCharacter>(CharacterOwner);
}

void UBTService_ShieldStatus::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	OwnerComp.GetBlackboardComponent()->SetValueAsBool("ShieldEquipped", BossOwner->bShieldEquipped);
}
