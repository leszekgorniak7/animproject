// Copyright by LG7


#include "BTTask_AreaAttack.h"
#include "BossCharacter.h"
#include "BossAIController.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UBTTask_AreaAttack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	BossAIController = Cast<ABossAIController>(OwnerComp.GetAIOwner());
	BossCharacter = Cast<ABossCharacter>(BossAIController->GetPawn());

	BossCharacter->EquipShield(false);
	BossAIController->ChaseTimer = 0.f;
	BossAIController->StopMovement();

	AreaAttackTimer = 0.f;

	BossCharacter->AreaAttack();	

	return EBTNodeResult::InProgress;
}

void UBTTask_AreaAttack::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	AreaAttackTimer += DeltaSeconds;

	if (AreaAttackTimer > AreaAttackTime)
	{
		BossCharacter->bIsAttacking = false;
		BossAIController->OnAbilityFinished();
	}
}

