// Copyright by LG7


#include "Behavior/Tasks/BTTask_BossBase.h"

UBTTask_BossBase::UBTTask_BossBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bNotifyTick = true;
	bNotifyTaskFinished = false;
	bIgnoreRestartSelf = false;
}