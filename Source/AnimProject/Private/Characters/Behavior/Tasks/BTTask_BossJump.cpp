// Copyright by LG7


#include "BTTask_BossJump.h"
#include "BossCharacter.h"
#include "BossAIController.h"


EBTNodeResult::Type UBTTask_BossJump::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	BossAIController = Cast<ABossAIController>(OwnerComp.GetAIOwner());
	BossCharacter = Cast<ABossCharacter>(BossAIController->GetPawn());

	BossCharacter->Jump();
	BossCharacter->bIsJumping = true;

	return EBTNodeResult::InProgress;
}
