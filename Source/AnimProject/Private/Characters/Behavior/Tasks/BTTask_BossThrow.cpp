// Copyright by LG7


#include "BTTask_BossThrow.h"
#include "BossCharacter.h"
#include "BossAIController.h"
#include "BossMovementComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Throwable.h"
#include "Navigation/PathFollowingComponent.h"

EBTNodeResult::Type UBTTask_BossThrow::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	BossAIController = Cast<ABossAIController>(OwnerComp.GetAIOwner());
	BossCharacter = Cast<ABossCharacter>(BossAIController->GetPawn());
	BossMovementComponent = BossCharacter->BossMovement;

	BossCharacter->GetCharacterMovement()->MaxWalkSpeed = BossMovementComponent->ChaseSpeed;
	ThrowPhase = 0;
	BossCharacter->EquipShield(false);
	BossCharacter->bIsAttacking = true;
	
	BlackboardComponent = OwnerComp.GetBlackboardComponent();

	ThrowableObject = Cast<AActor>(BlackboardComponent->GetValueAsObject(BossAIController->ThrowableObjectKey));

	return EBTNodeResult::InProgress;
}

void UBTTask_BossThrow::ActualThrow()
{
	BossCharacter->ThrowObject(ThrowableObject);
	bPickedUpObject = false;
	BossCharacter->GetCharacterMovement()->MaxWalkSpeed = BossMovementComponent->StandardSpeed;
	BlackboardComponent->ClearValue(BossAIController->ThrowableObjectKey);
	BossAIController->OnAbilityFinished();
}

void UBTTask_BossThrow::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	switch (ThrowPhase)
	{
		case 0:
		{
			BossAIController->SetFocus(ThrowableObject);
			if (BossAIController->MoveToActor(ThrowableObject, 200.f, true, true, true, nullptr, true) == EPathFollowingRequestResult::AlreadyAtGoal)
			{
				ThrowPhase = 1;
			}
			break;
		}
		case 1:
		{
			if (!bPickedUpObject)
			{
				bPickedUpObject = true;
				Cast<IThrowable>(ThrowableObject)->Execute_OnPickedUp(ThrowableObject);

				FAttachmentTransformRules AttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, false);
				ThrowableObject->AttachToComponent(BossCharacter->GetMesh(), AttachmentTransformRules, "throw_Socket");
				BossAIController->SetFocus(BossAIController->Target);

				FTimerDelegate TimerDel;
				FTimerHandle TimerHandle;
				TimerDel.BindUFunction(this, FName("ActualThrow"));
				GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDel, ThrowWaitTime, false);
			}
			break;
		}
	}
}

