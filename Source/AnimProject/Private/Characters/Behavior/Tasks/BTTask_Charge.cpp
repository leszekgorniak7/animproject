// Copyright by LG7

#include "BTTask_Charge.h"
#include "BossCharacter.h"
#include "BossAIController.h"
#include "BossMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"

void UBTTask_Charge::TryDealDamage()
{
	if (!bChargeDamageDealt && BossAIController->CheckMelee(BossAIController->Target))
	{
		bChargeDamageDealt = true;
		BossCharacter->DealChargeDamage();
	}
}

void UBTTask_Charge::FinishCharge(UBehaviorTreeComponent& OwnerComp)
{
	bChargeDamageDealt = false;
	ChargeWaitTimer = 0.f;
	ChargePhase = 0;
	BossCharacter->bIsAttacking = false;

	BossAIController->OnAbilityFinished();
}

EBTNodeResult::Type UBTTask_Charge::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	BossAIController = Cast<ABossAIController>(OwnerComp.GetAIOwner());
	BossCharacter = Cast<ABossCharacter>(BossAIController->GetPawn());

	BossCharacter->EquipShield(false);
	BossAIController->StopMovement();
	ChargeWaitTimer = 0.f;
	bChargeDamageDealt = false;
	ChargePhase = 0;
	BossCharacter->PlayAnimMontage(ChargePrepMontage);
	BossCharacter->bIsAttacking = true;

	return EBTNodeResult::InProgress;
}

void UBTTask_Charge::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	switch (ChargePhase)
	{
		case 0:
		{
			ChargeWaitTimer += DeltaSeconds;
			if (ChargeWaitTimer > ChargeWaitTime)
				ChargePhase = 1;

			break;
		}
		case 1:
		{
			BossAIController->ClearFocus(EAIFocusPriority::Gameplay);
			BossCharacter->ChargeAttack();
			ChargeTimer = 0.f;
			ChargePhase = 2;

			break;
		}
		case 2:
		{
			ChargeTimer += DeltaSeconds;
			if (ChargeTimer < ForwardChargeTime)
			{
				TryDealDamage();
			}
			else
			{
				ChargeTimer = 0.f;
				int32 RandomChance = UKismetMathLibrary::RandomIntegerInRange(0, 1);

				switch (RandomChance)
				{
					case 0:
					{
						OwnerComp.GetBlackboardComponent()->SetValueAsBool("ShouldCircle", true);
						break;
					}
					case 1:
					{
						OwnerComp.GetBlackboardComponent()->SetValueAsBool("ShouldRetreat", true);
						break;
					}
				}
				FinishCharge(OwnerComp);
			}

			break;
		}
	}
}