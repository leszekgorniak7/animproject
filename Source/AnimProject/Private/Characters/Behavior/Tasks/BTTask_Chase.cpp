// Copyright by LG7

#include "Behavior/Tasks/BTTask_Chase.h"
#include "BossCharacter.h"
#include "BossAIController.h"
#include "BossMovementComponent.h"
#include "Engine/World.h"

EBTNodeResult::Type UBTTask_Chase::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	BossAIController = Cast<ABossAIController>(OwnerComp.GetAIOwner());
	BossCharacter = Cast<ABossCharacter>(BossAIController->GetPawn());

	BossCharacter->EquipShield(false);
	BossAIController->RetreatTimer = 0.f;
	BossAIController->bIsChasing = true;

	BossMovementComponent = BossCharacter->BossMovement;

	return EBTNodeResult::InProgress;
}

void UBTTask_Chase::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	BossMovementComponent->FollowTarget();
	BossAIController->ChaseTimer += GetWorld()->GetDeltaSeconds();
}

EBTNodeResult::Type UBTTask_Chase::AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::AbortTask(OwnerComp, NodeMemory);		

	BossAIController->bIsChasing = false;

	return EBTNodeResult::Aborted;
}


