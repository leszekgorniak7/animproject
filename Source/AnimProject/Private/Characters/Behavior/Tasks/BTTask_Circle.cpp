// Copyright by LG7


#include "Behavior/Tasks/BTTask_Circle.h"
#include "BossCharacter.h"
#include "BossAIController.h"
#include "BossMovementComponent.h"
#include "Engine/World.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/KismetMathLibrary.h"

EBTNodeResult::Type UBTTask_Circle::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	BossAIController = Cast<ABossAIController>(OwnerComp.GetAIOwner());
	BossCharacter = Cast<ABossCharacter>(BossAIController->GetPawn());
	BossMovementComponent = BossCharacter->BossMovement;

	BossAIController->RetreatTimer = 0.f;
	BossAIController->bCircleRight = UKismetMathLibrary::RandomBool();

	if (UKismetMathLibrary::RandomFloatInRange(0.f, 1.f) < ShieldChance)
		BossCharacter->EquipShield(true);

	return EBTNodeResult::InProgress;
}

void UBTTask_Circle::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	BossMovementComponent->Circle(BossAIController->bCircleRight);
	BossAIController->RetreatTimer += GetWorld()->GetDeltaSeconds();

	if (BossAIController->RetreatTimer > BossAIController->RetreatTime)
	{
		OwnerComp.GetBlackboardComponent()->SetValueAsBool("ShouldCircle", false);
	}
}

