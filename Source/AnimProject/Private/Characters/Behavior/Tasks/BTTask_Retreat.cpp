// Copyright by LG7


#include "Behavior/Tasks/BTTask_Retreat.h"
#include "BossCharacter.h"
#include "BossAIController.h"
#include "BossMovementComponent.h"
#include "Engine/World.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/KismetMathLibrary.h"

EBTNodeResult::Type UBTTask_Retreat::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	BossAIController = Cast<ABossAIController>(OwnerComp.GetAIOwner());
	BossCharacter = Cast<ABossCharacter>(BossAIController->GetPawn());
	BossMovementComponent = BossCharacter->BossMovement;

	BossAIController->RetreatTimer = 0.f;
	
	if (OwnerComp.GetBlackboardComponent()->GetValueAsFloat("DistanceToTarget") < BossAIController->LongDistance)
	{
		if (UKismetMathLibrary::RandomFloatInRange(0.f, 1.f) < ShieldChance)
			BossCharacter->EquipShield(true);
	}
	else
	{
		OwnerComp.GetBlackboardComponent()->SetValueAsBool("ShouldRetreat", false);
		OwnerComp.GetBlackboardComponent()->SetValueAsBool("ShouldCircle", true);
	}

	return EBTNodeResult::InProgress;
}

void UBTTask_Retreat::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	BossMovementComponent->Retreat();
	BossAIController->RetreatTimer += GetWorld()->GetDeltaSeconds();

	if (BossAIController->RetreatTimer > BossAIController->RetreatTime)
	{
		OwnerComp.GetBlackboardComponent()->SetValueAsBool("ShouldRetreat", false);
	}
}

