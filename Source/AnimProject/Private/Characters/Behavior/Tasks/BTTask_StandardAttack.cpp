// Copyright by LG7

#include "BTTask_StandardAttack.h"
#include "BossCharacter.h"
#include "BossAIController.h"
#include "GlobalFunctionLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UBTTask_StandardAttack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	BossAIController = Cast<ABossAIController>(OwnerComp.GetAIOwner());
	BossCharacter = Cast<ABossCharacter>(BossAIController->GetPawn());

	BossCharacter->EquipShield(false);
	BossAIController->RetreatTimer = 0.f;
	BossAIController->ChaseTimer = 0.f;
	BossAIController->StopMovement();
	BossAIController->ClearFocus(EAIFocusPriority::Gameplay);

	int32 AttackRoll = UGlobalFunctionLibrary::ThrowDice(100);

	if (AttackRoll <= ChanceForFastAttackFirst * 100)
		BossCharacter->AttackCount = 1;
	else
		BossCharacter->AttackCount = 0;

	int32 PostAttackAction = UGlobalFunctionLibrary::ThrowDice(3);
	switch (PostAttackAction)
	{
		case 1:
		{
			break;
		}
		case 2:
		{
			OwnerComp.GetBlackboardComponent()->SetValueAsBool("ShouldCircle", true);
			break;
		}
		case 3:
		{
			OwnerComp.GetBlackboardComponent()->SetValueAsBool("ShouldRetreat", true);
			break;
		}
	}

	return EBTNodeResult::InProgress;
}

void UBTTask_StandardAttack::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	BossCharacter->StandardAttack();
}