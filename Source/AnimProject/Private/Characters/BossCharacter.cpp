// Fill out your copyright notice in the Description page of Project Settings.


#include "BossCharacter.h"
#include "HealthComponent.h"
#include "Components/BoxComponent.h"
#include "MyPlayerCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/Classes/Engine/World.h"
#include "TimerManager.h"
#include "BaseAIController.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BossMovementComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BossAIController.h"


ABossCharacter::ABossCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	BossMovement = CreateDefaultSubobject<UBossMovementComponent>("BossMovement");
	ShieldCollisionRef = CreateDefaultSubobject<UBoxComponent>("ShieldCollision");
	ShieldCollisionRef->SetupAttachment(GetMesh());
}

void ABossCharacter::EquipShield(bool Equip)
{
	bShieldEquipped = Equip;
	if (bShieldEquipped)
	{
		if (GetDistanceTo(AIController->Target) < SafeShieldDistance)
		{
			FVector DirectionVector = UKismetMathLibrary::GetDirectionUnitVector(GetActorLocation(), AIController->Target->GetActorLocation());
			FVector LaunchVector = FVector(DirectionVector.X * ShieldPushMultiplier, DirectionVector.Y * ShieldPushMultiplier, 100.f);
			
			ACharacter* TargetCharacter = Cast<ACharacter>(AIController->Target);			
			TargetCharacter->LaunchCharacter(LaunchVector, true, true);
			
			FTimerDelegate TimerDel;			
			TimerDel.BindUFunction(this, FName("EnableShieldCollision"), true);
			GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDel, ShieldCollisionDelay, false);
		}
		else
		{
			EnableShieldCollision(true);
		}
	}
	else
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
		EnableShieldCollision(false);
	}
}

void ABossCharacter::EnableShieldCollision(bool Enable)
{
	if (Enable)
	{
		ShieldCollisionRef->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Block);
	}
	else
	{
		ShieldCollisionRef->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	}
}

void ABossCharacter::ComboAttackSave()
{
	if (bSaveAttack)
	{
		bSaveAttack = false;
		OnComboAttackSave();
	}
}

void ABossCharacter::ResetCombo()
{
	bIsAttacking = false;
	bSaveAttack = false;
	AttackCount = 0;
}

void ABossCharacter::OnDamageAnim(bool bStarted)
{
	HealthComponent->bIsTakingDamage = bStarted;
	//UE_LOG(LogTemp, Warning, TEXT("Is Taking Damage: %s"), HealthComponent->bIsTakingDamage ? "1" : "0");
}

void ABossCharacter::OnJumpEnd()
{
	Super::OnJumpEnd();
	UGameplayStatics::PlayWorldCameraShake(this, JumpLandShake, GetActorLocation(), 0.f, 2000.f);
	BossAIController->OnAbilityFinished();
}

void ABossCharacter::BeginPlay()
{
	Super::BeginPlay();
	BossAIController = Cast<ABossAIController>(AIController);
	PlayerCharacter->OnCharacterDeath.AddDynamic(this, &ABossCharacter::OnPlayerKilled);
	EquipShield(false);
}
