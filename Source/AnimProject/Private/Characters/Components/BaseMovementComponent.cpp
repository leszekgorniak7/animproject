// Copyright by LG7


#include "BaseMovementComponent.h"
#include "BaseCharacter.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Engine/Classes/Kismet/KismetMathLibrary.h"
#include "AIController.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Classes/Engine/World.h"
#include "Player\MyPlayerCharacter.h"
#include "BaseAIController.h"
#include "BaseCharacter.h"

// Sets default values for this component's properties
UBaseMovementComponent::UBaseMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBaseMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	CharacterReference = Cast<ABaseCharacter>(GetOwner());
	
}

void UBaseMovementComponent::BasicMovementSetup(float Speed, bool OrientRotationToMovement, bool UseControllerDesiredRotation)
{
	CharacterReference->GetCharacterMovement()->MaxWalkSpeed = Speed;
	CharacterReference->GetCharacterMovement()->bOrientRotationToMovement = OrientRotationToMovement;
	CharacterReference->GetCharacterMovement()->bUseControllerDesiredRotation = UseControllerDesiredRotation;
}

void UBaseMovementComponent::FollowTarget()
{
	if (bChaseSpeed) BasicMovementSetup(ChaseSpeed, false, true);
	else BasicMovementSetup(StandardSpeed, false, true);

	UAIBlueprintHelperLibrary::GetAIController(CharacterReference)->MoveToActor(CharacterReference->AIController->Target, 5.f, false, true, true, nullptr, true);
	UAIBlueprintHelperLibrary::GetAIController(CharacterReference)->SetFocus(CharacterReference->AIController->Target);
}

void UBaseMovementComponent::Retreat()
{
	BasicMovementSetup(StandardSpeed, false, true);

	CharacterReference->AddMovementInput(UKismetMathLibrary::GetDirectionUnitVector(CharacterReference->AIController->Target->GetActorLocation(),
		CharacterReference->GetActorLocation()), 1.f);
	UAIBlueprintHelperLibrary::GetAIController(CharacterReference)->SetFocus(CharacterReference->AIController->Target);
}

void UBaseMovementComponent::Circle(bool Right)
{
	BasicMovementSetup(StandardSpeed, false, true);

	float InputScale;
	if (Right)
		InputScale = 1.f;
	else
		InputScale = -1.f;

	CharacterReference->AddMovementInput(CharacterReference->GetActorRightVector(), InputScale);
	UAIBlueprintHelperLibrary::GetAIController(CharacterReference)->SetFocus(CharacterReference->AIController->Target);
}

float UBaseMovementComponent::GetLookAtTargetYaw(AActor* EnemyTarget) const
{
	return UKismetMathLibrary::FindLookAtRotation(CharacterReference->GetActorLocation(), EnemyTarget->GetActorLocation()).Yaw;
}

