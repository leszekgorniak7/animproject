// Copyright by LG7


#include "BossMovementComponent.h"
#include "Characters/BossCharacter.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Engine/Classes/Kismet/KismetMathLibrary.h"
#include "AIController.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Classes/Engine/World.h"
#include "TimerManager.h"
#include "Player\MyPlayerCharacter.h"
#include "BaseCharacter.h"
#include "BaseAIController.h"

// Sets default values for this component's properties
UBossMovementComponent::UBossMovementComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UBossMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	BossCharacterReference = Cast<ABossCharacter>(GetOwner());
	
}

void UBossMovementComponent::SwitchToChaseSpeed()
{
	if (!bChaseSpeed)
	{
		bChaseSpeed = true;
		float ChaseDuration = UKismetMathLibrary::RandomFloatInRange(MinChaseDuration, MaxChaseDuration);

		FTimerDelegate TimerDel;
		FTimerHandle TimerHandle;
		TimerDel.BindUFunction(this, FName("SwitchToStandardSpeed"));
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDel, ChaseDuration, false);
	}
}

void UBossMovementComponent::SwitchToStandardSpeed()
{
	bChaseSpeed = false;
}

void UBossMovementComponent::MoveToJumpLocation()
{
	FVector NewLocation = 
		UKismetMathLibrary::VInterpTo(BossCharacterReference->GetActorLocation(), BossCharacterReference->AIController->Target->GetActorLocation(), UGameplayStatics::GetWorldDeltaSeconds(this), JumpInterpSpeed);
	FRotator NewRotation = 
		UKismetMathLibrary::MakeRotator(BossCharacterReference->GetActorRotation().Roll, BossCharacterReference->GetActorRotation().Pitch, GetLookAtTargetYaw(BossCharacterReference->AIController->Target));

	BossCharacterReference->SetActorLocationAndRotation(NewLocation, NewRotation);
}

void UBossMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (BossCharacterReference->bIsJumping)
		MoveToJumpLocation();
}

