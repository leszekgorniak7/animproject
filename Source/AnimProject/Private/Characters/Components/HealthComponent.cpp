// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "BaseCharacter.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"

UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}


float UHealthComponent::GetCurrentHealth() const
{
	return CurrentHealth;
}

float UHealthComponent::GetHealthPercentage() const
{
	return CurrentHealth/MaxHealth;
}

bool UHealthComponent::HasLowHealth() const
{
	return GetHealthPercentage() < LowHealthTreshold;
}

void UHealthComponent::AddHealth(float Amount)
{
	CurrentHealth = UKismetMathLibrary::Clamp(CurrentHealth + Amount, 0, MaxHealth);
}

void UHealthComponent::MakeDead()
{
	CurrentHealth = 0;
	bIsAlive = false;
	
	if (DeathAnimation)
	{
		CharacterOwner->PlayAnimMontage(DeathAnimation, 1.f, FName(""));
	}
	
	OnDeath.Broadcast();
}

void UHealthComponent::HandleDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (bIsAlive && !bIsInvincible)
	{
		CurrentHealth -= Damage;

		if (GetHealthPercentage() < LowHealthTreshold)
		{
			OnLowHealth.Broadcast();
		}

		if (bDamageInterruptsAttack || !CharacterOwner->bIsAttacking)
		{
			if (HitAnimation)
			{
				CharacterOwner->PlayAnimMontage(HitAnimation, 1.f, FName(""));
			}
			
			CharacterOwner->bIsAttacking = false;
		}
		
		if (CurrentHealth <= 0)
		{
			MakeDead();
		}
	}
}

void UHealthComponent::SetInvincible(bool Invincible)
{
	bIsInvincible = Invincible;
}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = MaxHealth;
	CharacterOwner = Cast<ABaseCharacter>(GetOwner());
	CharacterOwner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::HandleDamage);
}

