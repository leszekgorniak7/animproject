//Copyright by LG7


#include "BaseAIController.h"
#include "BaseCharacter.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Utils/GlobalFunctionLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GameActors/ThrowableObjectPickup.h"

bool ABaseAIController::CheckMelee(AActor* EnemyTarget) const
{
	return PossessedCharacter->IsTargetInMeleeRange(EnemyTarget);
}

bool ABaseAIController::FindThrowableObject()
{
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectType = {EObjectTypeQuery::ObjectTypeQuery2};
	const TArray< AActor* > ActorsToIgnore = {};
	TArray< class AActor* > OutActors;
	
	if (UKismetSystemLibrary::SphereOverlapActors(this, PossessedCharacter->GetActorLocation(), ThrowableObjectSearchRadius, ObjectType,
		AThrowableObjectPickup::StaticClass(), ActorsToIgnore, OutActors))
	{
		GetBlackboardComponent()->SetValueAsObject(ThrowableObjectKey, UGlobalFunctionLibrary::FindNearestActor(PossessedCharacter, OutActors));
		return true;
	}
	
	else
		return false;
}

void ABaseAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	PossessedCharacter = Cast<ABaseCharacter>(InPawn);
}

