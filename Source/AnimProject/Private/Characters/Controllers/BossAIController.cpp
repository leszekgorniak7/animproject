// Copyright by LG7


#include "BossAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BossCharacter.h"

bool ABossAIController::CheckMelee(AActor* EnemyTarget) const
{
	return PossessedCharacter->IsTargetInMeleeRange(EnemyTarget) && !BossReference->bShieldEquipped;
}

void ABossAIController::OnAbilityFinished()
{
	GetBlackboardComponent()->SetValueAsBool(UsingAbilityKey, false);
	RunSituationCheck();
}

void ABossAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	BossReference = Cast<ABossCharacter>(PossessedCharacter);
}
