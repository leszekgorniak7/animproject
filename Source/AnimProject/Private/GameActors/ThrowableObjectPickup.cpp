// Copyright by LG7


#include "ThrowableObjectPickup.h"

AThrowableObjectPickup::AThrowableObjectPickup()
{
	PrimaryActorTick.bCanEverTick = true;

}

void AThrowableObjectPickup::OnPickedUp_Implementation()
{
	UStaticMeshComponent* Mesh = FindComponentByClass<UStaticMeshComponent>();
	if (Mesh)
	{
		Cast<UPrimitiveComponent>(Mesh)->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

