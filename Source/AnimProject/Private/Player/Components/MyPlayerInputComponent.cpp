// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/Components/MyPlayerInputComponent.h"
#include "MyPlayerCharacter.h"
#include "UI/PlayerHUD.h"
#include "Engine/Classes/Kismet/KismetMathLibrary.h"

UMyPlayerInputComponent::UMyPlayerInputComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UMyPlayerInputComponent::InitializeInput()
{
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputComponent)
	{
		InputComponent->BindAxis("MoveForward", this, &UMyPlayerInputComponent::MoveForward);
		InputComponent->BindAxis("MoveRight", this, &UMyPlayerInputComponent::MoveRight);
		InputComponent->BindAxis("LookUp", this, &UMyPlayerInputComponent::LookUp);
		InputComponent->BindAxis("LookUpRate", this, &UMyPlayerInputComponent::LookUpRate);
		InputComponent->BindAxis("Turn", this, &UMyPlayerInputComponent::Turn);
		InputComponent->BindAxis("TurnRate", this, &UMyPlayerInputComponent::TurnRate);

		InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &UMyPlayerInputComponent::Jump);
		InputComponent->BindAction("Jump", EInputEvent::IE_Released, this, &UMyPlayerInputComponent::StopJumping);
		InputComponent->BindAction("LockOnTarget", EInputEvent::IE_Pressed, this, &UMyPlayerInputComponent::LockOnTarget);
		InputComponent->BindAction("Dodge", EInputEvent::IE_Pressed, this, &UMyPlayerInputComponent::Dodge);
		InputComponent->BindAction("FastAttack", EInputEvent::IE_Pressed, this, &UMyPlayerInputComponent::FastAttack);
		InputComponent->BindAction("StrongAttack", EInputEvent::IE_Pressed, this, &UMyPlayerInputComponent::StrongAttack);

		UE_LOG(LogTemp, Warning, TEXT("InputComponent initialized"));
	}
}

void UMyPlayerInputComponent::MoveForward(float Value)
{
	if (!PlayerCharacter->bIsDodging) 
		PlayerCharacter->AddMovementInput(UKismetMathLibrary::GetForwardVector(FRotator(0.f, PlayerCharacter->GetControlRotation().Yaw, 0.f)), Value);
}

void UMyPlayerInputComponent::MoveRight(float Value)
{
	if (!PlayerCharacter->bIsDodging) 
		PlayerCharacter->AddMovementInput(UKismetMathLibrary::GetRightVector(FRotator(0.f, PlayerCharacter->GetControlRotation().Yaw, 0.f)), Value);
}

void UMyPlayerInputComponent::LookUp(float Value)
{
	PlayerCharacter->AddControllerPitchInput(Value);
}

void UMyPlayerInputComponent::LookUpRate(float Value)
{
	PlayerCharacter->AddControllerPitchInput(Value * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void UMyPlayerInputComponent::Turn(float Value)
{
	PlayerCharacter->AddControllerYawInput(Value);
}

void UMyPlayerInputComponent::TurnRate(float Value)
{
	PlayerCharacter->AddControllerYawInput(Value * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void UMyPlayerInputComponent::Jump()
{
	PlayerCharacter->Jump();
}

void UMyPlayerInputComponent::StopJumping()
{
	PlayerCharacter->StopJumping();
}

void UMyPlayerInputComponent::LockOnTarget()
{
	PlayerCharacter->ToggleLockOnTarget();
}

void UMyPlayerInputComponent::Dodge()
{
	PlayerCharacter->Dodge();
}

void UMyPlayerInputComponent::FastAttack()
{
	PlayerCharacter->FastAttack();
}

void UMyPlayerInputComponent::StrongAttack()
{
	PlayerCharacter->StrongAttack();
}

void UMyPlayerInputComponent::BeginPlay()
{
	Super::BeginPlay();
	PlayerCharacter = Cast<AMyPlayerCharacter>(GetOwner());
	
}

void UMyPlayerInputComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

