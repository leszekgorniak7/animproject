// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPlayerCharacter.h"
#include "UI/PlayerHUD.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "HealthComponent.h"
#include "Player/Components/MyPlayerInputComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "GlobalFunctionLibrary.h"

AMyPlayerCharacter::AMyPlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AMyPlayerCharacter::TryLockOnTarget()
{
	const TArray< TEnumAsByte< EObjectTypeQuery > > ObjectTypes = { EObjectTypeQuery::ObjectTypeQuery3 }; //Pawn
	UClass* ActorClassFilter = nullptr;
	const TArray< AActor* > ActorsToIgnore = { this };
	TArray< class AActor* > OutActors;
	UKismetSystemLibrary::SphereOverlapActors(this, GetActorLocation(), 5000.f, ObjectTypes, ActorClassFilter, ActorsToIgnore, OutActors);

	TArray<AActor*> ActorsInFront;
	for (int i = 0; i < OutActors.Num(); i++)
	{
		if (UKismetMathLibrary::Abs
		(UKismetMathLibrary::FindLookAtRotation(FollowCameraComp->GetComponentLocation(), OutActors[i]->GetActorLocation()).Yaw - FollowCameraComp->GetComponentRotation().Yaw) < 45.f)
		{
			ActorsInFront.Add(OutActors[i]);
		}
	}

	TArray<AActor*> ActorsInSight;
	FHitResult Hit(ForceInit);

	for (int i = 0; i < ActorsInFront.Num(); i++)
	{
		FVector Start = FollowCameraComp->GetComponentLocation();
		FVector End = ActorsInFront[i]->GetActorLocation();
		FCollisionQueryParams CollisionParams;
		CollisionParams.AddIgnoredActor(this);

		GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_GameTraceChannel1 /* LockTarget */, CollisionParams);

		if (Hit.GetActor() == ActorsInFront[i])
		{
			ActorsInSight.Add(ActorsInFront[i]);
		}
	}

	if (ActorsInSight.Num() > 0)
	{
		APawn* HitPawn = Cast<APawn>(UGlobalFunctionLibrary::FindNearestActor(this, ActorsInSight));
		if (HitPawn)
		{
			bLockedOnTarget = true;
			FocusPawn = HitPawn;
			FocusPoint = Hit.GetComponent();
		}
	}
}

void AMyPlayerCharacter::DisableLockOnTarget()
{
	bLockedOnTarget = false;
	CharacterMovementComponent->bOrientRotationToMovement = true;
	bUseControllerRotationYaw = false;
	FocusPawn = nullptr;
	PlayerHUD->HideLockMarker();
}

void AMyPlayerCharacter::ToggleLockOnTarget()
{
	if (bLockedOnTarget)
		DisableLockOnTarget();
	else
		TryLockOnTarget();
}

void AMyPlayerCharacter::DisableLockOnIfTargetKilled(APawn* KilledPawn)
{
	if (FocusPawn && FocusPawn == KilledPawn)
		DisableLockOnTarget();
}

void AMyPlayerCharacter::FollowLockedTarget()
{
	FRotator TargetLookAtRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), FocusPawn->GetActorLocation());
	FRotator LockedOnRotation = FRotator(PlayerController->GetControlRotation().Pitch, TargetLookAtRotation.Yaw, PlayerController->GetControlRotation().Roll);
	PlayerController->SetControlRotation(LockedOnRotation);
	
	PlayerHUD->SetLockMarkerPosition(FocusPoint->GetComponentLocation());

	CharacterMovementComponent->bOrientRotationToMovement = false;
	bUseControllerRotationYaw = true;
}

void AMyPlayerCharacter::OnDodgeInvincibilityEnd()
{
	HealthComponent->SetInvincible(false);
}

void AMyPlayerCharacter::OnDodgeEnd()
{
	bIsDodging = false;
	CharacterMovementComponent->BrakingFriction = 2.f;
}

void AMyPlayerCharacter::OnDamageAnim(bool bStarted)
{
	HealthComponent->bIsTakingDamage = bStarted;
	if (!bStarted) bIsDodging = false;
}

void AMyPlayerCharacter::OnDodgeInvincibilityStart()
{
	HealthComponent->SetInvincible(true);
}

void AMyPlayerCharacter::ResetCombo()
{
	bIsAttacking = false;
}

void AMyPlayerCharacter::Dodge()
{
	if (!CharacterMovementComponent->IsFalling() && !bIsDodging && !bIsAttacking && !HealthComponent->bIsTakingDamage)
	{
		bIsDodging = true;
		CharacterMovementComponent->BrakingFrictionFactor = 0.7f;

		PlayAnimMontage(DodgeAnimation, 1.f, FName(""));

		FVector IdleImpulse =
			FVector(UKismetMathLibrary::GetForwardVector(FRotator(0, GetControlRotation().Yaw, 0))) * -400000.f;

		FVector DodgeMovementVector = CharacterMovementComponent->GetLastInputVector();
		DodgeMovementVector.GetSafeNormal(1.0f);
		DodgeMovementVector.Normalize(1.0f);

		FVector MovementImpulse =
			DodgeMovementVector * 400000.f;

		if (CharacterMovementComponent->GetLastInputVector().Size() > 0) 
			CharacterMovementComponent->AddImpulse(MovementImpulse);
		else
			CharacterMovementComponent->AddImpulse(IdleImpulse);
	}
}

void AMyPlayerCharacter::FastAttack()
{
	if (!bIsDodging && !HealthComponent->bIsTakingDamage && !bIsAttacking)
	{
		AttackType = EAttackType::Fast;
		bIsAttacking = true;
		switch (FastAttackCount)
		{
			case 0:
			{
				FastAttackCount = 1;
				PlayAnimMontage(FastAttack_01_Animation, 1.f, FName(""));
				break;
			}
			case 1:
			{
				FastAttackCount = 0;
				PlayAnimMontage(FastAttack_02_Animation, 1.f, FName(""));
				break;
			}
		}
	}
}

void AMyPlayerCharacter::StrongAttack()
{
	if (!bIsDodging && !HealthComponent->bIsTakingDamage && !bIsAttacking)
	{
		AttackType = EAttackType::Strong;
		bIsAttacking = true;
		switch (StrongAttackCount)
		{
			case 0:
			{
				StrongAttackCount = 1;
				PlayAnimMontage(StrongAttack_01_Animation, 1.f, FName(""));
				break;
			}
			case 1:
			{
				StrongAttackCount = 0;
				PlayAnimMontage(StrongAttack_02_Animation, 1.f, FName(""));
				break;
			}
		}
	}
}

void AMyPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	CharacterMovementComponent = GetCharacterMovement();
	PlayerController = UGameplayStatics::GetPlayerController(this, 0);
}

void AMyPlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bLockedOnTarget)
	{
		FollowLockedTarget();
	}
}

void AMyPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	MyPlayerInputComponent = FindComponentByClass<UMyPlayerInputComponent>();
	MyPlayerInputComponent->InitializeInput();
}

