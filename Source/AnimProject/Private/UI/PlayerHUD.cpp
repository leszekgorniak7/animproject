// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerHUD.h"
#include "LockTarget.h"
#include "WidgetLayoutLibrary.h"
#include "CanvasPanelSlot.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

void UPlayerHUD::SetLockMarkerPosition(FVector TargetPosition)
{
	if (LockTargetWidget)
	{
		LockTargetWidget->SetVisibility(ESlateVisibility::Visible);

		FVector2D LockTargetPosition;
		FVector2D ScreenPosition;

		UGameplayStatics::ProjectWorldToScreen(UGameplayStatics::GetPlayerController(this, 0), TargetPosition, ScreenPosition);
		LockTargetPosition = ScreenPosition * UKismetMathLibrary::MultiplyMultiply_FloatFloat(UWidgetLayoutLibrary::GetViewportScale(this), -1.f);
		UWidgetLayoutLibrary::SlotAsCanvasSlot(LockTargetWidget)->SetPosition(LockTargetPosition);
	}
}

void UPlayerHUD::HideLockMarker()
{
	if (LockTargetWidget)
	{
		LockTargetWidget->SetVisibility(ESlateVisibility::Hidden);
	}
}