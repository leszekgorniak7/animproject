// Copyright by LG7


#include "GlobalFunctionLibrary.h"
#include "Engine/Classes/Kismet/KismetMathLibrary.h"

AActor* UGlobalFunctionLibrary::FindNearestActor(AActor* ReferenceActor, TArray<AActor*> Actors)
{
	TArray<float> Distances;
	for (int i = 0; i < Actors.Num(); i++)
	{
		Distances.Add(ReferenceActor->GetDistanceTo(Actors[i]));
	}

	int MinDistanceIndex;
	float MinDistance;
	UKismetMathLibrary::MinOfFloatArray(Distances, MinDistanceIndex, MinDistance);

	return Actors[MinDistanceIndex];
}

int32 UGlobalFunctionLibrary::ThrowDice(int32 Sides)
{
	return UKismetMathLibrary::RandomIntegerInRange(1, Sides);
}

