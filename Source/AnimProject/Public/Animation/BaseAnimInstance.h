// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "Engine/Classes/GameFramework/PawnMovementComponent.h"
#include "Engine/Classes/GameFramework/CharacterMovementComponent.h"
#include "BaseAnimInstance.generated.h"

class ABaseCharacter;

UCLASS()
class ANIMPROJECT_API UBaseAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	virtual void NativeInitializeAnimation() override;
	virtual void NativeUpdateAnimation(float DeltaTime) override;

	virtual void OnMyNotifyBegin(FString Name);
	virtual void OnMyNotifyEnd(FString Name);
	virtual void HandleMyNotify(FString Name);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float Speed;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bIsInAir;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float Pitch;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float Roll;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float Yaw;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FRotator RotationLastTick;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float YawDelta;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bIsAccelerating;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bFullBody;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FVector MovementDirection;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float LeanIntensityScaling = 7;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float YawDeltaInterpSpeed = 6;

protected:
	UPawnMovementComponent * MovementComponent = nullptr;
	ABaseCharacter * CharacterOwner = nullptr;
	UCharacterMovementComponent * CharacterMovementComponent = nullptr;

private:
	APawn * PawnOwner = nullptr;
};
