// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseAnimInstance.h"
#include "BossAnimInstance.generated.h"

/**
 * 
 */

class ABossCharacter;

UCLASS()
class ANIMPROJECT_API UBossAnimInstance : public UBaseAnimInstance
{
	GENERATED_BODY()

public:
	virtual void NativeInitializeAnimation() override;
	virtual void NativeUpdateAnimation(float DeltaTime) override;

	UPROPERTY(BlueprintReadOnly)
	ABossCharacter* BossCharacter = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bShieldEquipped = false;	
};
