// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseAnimInstance.h"
#include "PlayerAnimInstance.generated.h"

/**
 * 
 */

class AMyPlayerCharacter;

UCLASS()
class ANIMPROJECT_API UPlayerAnimInstance : public UBaseAnimInstance
{
	GENERATED_BODY()

public:
	virtual void NativeInitializeAnimation() override;
	virtual void NativeUpdateAnimation(float DeltaTime) override;

	virtual void HandleMyNotify(FString Name) override;
	virtual void OnMyNotifyBegin(FString Name) override;
	virtual void OnMyNotifyEnd(FString Name) override;

private:
	AMyPlayerCharacter * PlayerCharacter = nullptr;
};
