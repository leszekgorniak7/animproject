// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Engine/Classes/GameFramework/CharacterMovementComponent.h"
#include "BaseCharacter.generated.h"

class UHealthComponent;
class AMyPlayerCharacter;
class ABaseAIController;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharacterDeath);

UCLASS()
class ANIMPROJECT_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ABaseCharacter();
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Base Character")
	FOnCharacterDeath OnCharacterDeath;
	
	/*Max angle (Yaw) to hit target within MeleeRadius*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Base Character")
	float MeleeAngle = 60.f;

	/*Max distance to hit target within MeleeAngle*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Base Character")
	float MeleeRadius = 200.f;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class UHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Base Character")
	bool bIsAttacking = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Base Character")
	bool bSaveAttack = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Base Character")
	bool bIsJumping = false;

	UFUNCTION(BlueprintPure, Category = "Base Character")
	float GetLookAtTargetYaw(AActor * Target) const;

	UFUNCTION(BlueprintPure, Category = "Base Character")
	bool IsTargetInMeleeRange(AActor * EnemyTarget) const;

	UFUNCTION(BlueprintImplementableEvent, Category = "Base Character")
	void HitCheck();

	virtual void ResetCombo() {};
	virtual void ComboAttackSave() {};
	virtual void OnDamageAnim(bool bStarted) {};
	virtual void OnJumpEnd();

	UPROPERTY(BlueprintReadOnly, Category = "BaseCharacter")
	AMyPlayerCharacter* PlayerCharacter = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "BaseCharacter")
	ABaseAIController* AIController = nullptr;

protected:
	virtual void BeginPlay() override;
	UCharacterMovementComponent * MovementComponent = nullptr;
};
