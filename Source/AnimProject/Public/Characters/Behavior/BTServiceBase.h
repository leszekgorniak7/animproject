// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTServiceBase.generated.h"

class ABaseCharacter;
class ABaseAIController;

UCLASS()
class ANIMPROJECT_API UBTServiceBase : public UBTService
{
	GENERATED_BODY()

public:
	UBTServiceBase();

protected:
	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	ABaseCharacter* CharacterOwner = nullptr;
	ABaseAIController* AIController = nullptr;
};
