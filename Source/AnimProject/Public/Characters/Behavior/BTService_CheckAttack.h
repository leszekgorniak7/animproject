// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Characters/Behavior/BTServiceBase.h"
#include "BTService_CheckAttack.generated.h"

/**
 * 
 */
UCLASS()
class ANIMPROJECT_API UBTService_CheckAttack : public UBTServiceBase
{
	GENERATED_BODY()

protected:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
};
