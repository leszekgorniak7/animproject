// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Characters/Behavior/BTServiceBase.h"
#include "BTService_CheckMelee.generated.h"

/**
 * 
 */
UCLASS()
class ANIMPROJECT_API UBTService_CheckMelee : public UBTServiceBase
{
	GENERATED_BODY()

protected:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
};
