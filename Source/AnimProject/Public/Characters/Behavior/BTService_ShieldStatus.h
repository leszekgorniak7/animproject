// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Characters/Behavior/BTServiceBase.h"
#include "BTService_ShieldStatus.generated.h"

class ABossCharacter;

UCLASS()
class ANIMPROJECT_API UBTService_ShieldStatus : public UBTServiceBase
{
	GENERATED_BODY()

protected:
	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

private:
	ABossCharacter* BossOwner = nullptr;
	
};
