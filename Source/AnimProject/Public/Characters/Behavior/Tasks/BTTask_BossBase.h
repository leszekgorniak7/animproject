// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_BossBase.generated.h"

class ABossAIController;
class ABossCharacter;

UCLASS()
class ANIMPROJECT_API UBTTask_BossBase : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTTask_BossBase(const FObjectInitializer& ObjectInitializer);

protected:
	ABossAIController* BossAIController = nullptr;
	ABossCharacter* BossCharacter = nullptr;
	
};
