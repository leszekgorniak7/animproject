// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Characters/Behavior/Tasks/BTTask_BossBase.h"
#include "BTTask_BossJump.generated.h"

UCLASS()
class ANIMPROJECT_API UBTTask_BossJump : public UBTTask_BossBase
{
	GENERATED_BODY()

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
};
