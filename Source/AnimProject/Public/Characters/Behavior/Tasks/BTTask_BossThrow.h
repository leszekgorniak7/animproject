// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Characters/Behavior/Tasks/BTTask_BossBase.h"
#include "BTTask_BossThrow.generated.h"

/**
 * 
 */
UCLASS()
class ANIMPROJECT_API UBTTask_BossThrow : public UBTTask_BossBase
{
	GENERATED_BODY()

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

public:
	UPROPERTY(EditAnywhere, Category = "Boss AI")
	float ThrowWaitTime = 1.f;

	UFUNCTION()
	void ActualThrow();

protected:
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

private:
	class UBossMovementComponent* BossMovementComponent = nullptr;
	class UBlackboardComponent* BlackboardComponent = nullptr;
	int32 ThrowPhase = 0;
	bool bPickedUpObject = false;
	AActor* ThrowableObject = nullptr;
	
};
