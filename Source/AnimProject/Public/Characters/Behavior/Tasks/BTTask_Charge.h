// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Characters/Behavior/Tasks/BTTask_BossBase.h"
#include "BTTask_Charge.generated.h"

class UBossMovementComponent;

UCLASS()
class ANIMPROJECT_API UBTTask_Charge : public UBTTask_BossBase
{
	GENERATED_BODY()

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

public:
	UPROPERTY(EditAnywhere, Category = "Boss AI")
	float ChargeWaitTime = 4.f;

	UPROPERTY(EditAnywhere, Category = "Boss AI")
	float ForwardChargeTime = 1.5f;

	UPROPERTY(EditAnywhere, Category = "Boss AI")
	UAnimMontage* ChargePrepMontage = nullptr;

protected:
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

private:
	
	void TryDealDamage();
	void FinishCharge(UBehaviorTreeComponent& OwnerComp);
	
	float ChargeWaitTimer = 0.f;
	int32 ChargePhase = 0;
	bool bChargeDamageDealt = false;
	float ChargeTimer = 0.f;
	FVector ChargeVector;
	FVector ChargeTargetLocation;
};
