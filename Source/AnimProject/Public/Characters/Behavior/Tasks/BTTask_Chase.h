// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Characters/Behavior/Tasks/BTTask_BossBase.h"
#include "BTTask_Chase.generated.h"

class UBossMovementComponent;

UCLASS()
class ANIMPROJECT_API UBTTask_Chase : public UBTTask_BossBase
{
	GENERATED_BODY()
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	virtual EBTNodeResult::Type AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:
	UBossMovementComponent* BossMovementComponent = nullptr;

};
