// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Characters/Behavior/Tasks/BTTask_BossBase.h"
#include "BTTask_Retreat.generated.h"

class UBossMovementComponent;

UCLASS()
class ANIMPROJECT_API UBTTask_Retreat : public UBTTask_BossBase
{
	GENERATED_BODY()

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

public:
	/*Chance to equip shield*/
	UPROPERTY(EditAnywhere, Category = "Boss AI")
	float ShieldChance = 0.5f;

protected:
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

private:
	UBossMovementComponent* BossMovementComponent = nullptr;
	
};
