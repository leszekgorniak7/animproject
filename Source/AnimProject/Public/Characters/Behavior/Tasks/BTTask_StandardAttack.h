// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Characters/Behavior/Tasks/BTTask_BossBase.h"
#include "BTTask_StandardAttack.generated.h"

UCLASS()
class ANIMPROJECT_API UBTTask_StandardAttack : public UBTTask_BossBase
{
	GENERATED_BODY()

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

public:
	/*Chance to start with Fast Attack*/
	UPROPERTY(EditAnywhere, Category = "Boss AI")
	float ChanceForFastAttackFirst = 0.2f;

protected:
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;	
};
