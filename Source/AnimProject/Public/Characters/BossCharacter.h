// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Characters\BaseCharacter.h"
#include "BossCharacter.generated.h"

class UBoxComponent;
class ABossAIController;
class UBossMovementComponent;
class AMyPlayerCharacter;

UCLASS()
class ANIMPROJECT_API ABossCharacter : public ABaseCharacter
{
	GENERATED_BODY()

public:
	ABossCharacter();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Boss Character")
	int32 AttackCount = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Boss Shield")
	bool bShieldEquipped = false;

	UPROPERTY(EditDefaultsOnly, Category = "Boss Shield")
	float SafeShieldDistance = 300.f;

	UPROPERTY(EditDefaultsOnly, Category = "Boss Shield")
	float ShieldPushMultiplier = 1000.f;

	UPROPERTY(EditDefaultsOnly, Category = "Boss Shield")
	float ShieldCollisionDelay = 0.4f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Boss Shield")
	UBoxComponent * ShieldCollisionRef = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Boss Movement")
	UBossMovementComponent* BossMovement = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Boss Jump")
	TSubclassOf<UCameraShakeBase> JumpLandShake = nullptr;

	UFUNCTION()
	void EnableShieldCollision(bool Enable);

	UFUNCTION(BlueprintCallable, Category = "Boss Shield")
	void EquipShield(bool Equip);
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Boss Character")
	void OnComboAttackSave();

	UFUNCTION(BlueprintImplementableEvent, Category = "Boss Character")
	void StandardAttack();

	UFUNCTION(BlueprintImplementableEvent, Category = "Boss Character")
	void ChargeAttack();

	UFUNCTION(BlueprintImplementableEvent, Category = "Boss Character")
	void DealChargeDamage();

	UFUNCTION(BlueprintImplementableEvent, Category = "Boss Character")
	void AreaAttack();

	UFUNCTION(BlueprintImplementableEvent, Category = "Boss Character")
	void ThrowObject(AActor* AttachedObject);

	UFUNCTION(BlueprintImplementableEvent, Category = "Boss Character")
	void OnPlayerKilled();

	virtual void OnJumpEnd() override;
	virtual void ComboAttackSave() override;
	virtual void ResetCombo() override;
	virtual void OnDamageAnim(bool bStarted) override;

	ABossAIController* BossAIController = nullptr;

protected:
	virtual void BeginPlay() override;

private:
	FTimerHandle TimerHandle;
};
