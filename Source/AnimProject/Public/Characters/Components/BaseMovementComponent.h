// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BaseMovementComponent.generated.h"

class ABaseCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ANIMPROJECT_API UBaseMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UBaseMovementComponent();

	UPROPERTY(BlueprintReadOnly, Category = "BaseMovement")
	ABaseCharacter* CharacterReference = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BaseMovement")
	float StandardSpeed = 200.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BaseMovement")
	float ChaseSpeed = 500.f;

	UFUNCTION(BlueprintCallable, Category = "BaseMovement")
	void FollowTarget();

	UFUNCTION(BlueprintCallable, Category = "BaseMovement")
	void Retreat();

	UFUNCTION(BlueprintCallable, Category = "BaseMovement")
	void Circle(bool Right);

protected:
	virtual void BeginPlay() override;

	void BasicMovementSetup(float Speed, bool OrientRotationToMovement, bool UseControllerDesiredRotation);
	float GetLookAtTargetYaw(AActor* EnemyTarget) const;
	bool bChaseSpeed = false;
		
};
