// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BaseMovementComponent.h"
#include "BossMovementComponent.generated.h"

class ABossCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ANIMPROJECT_API UBossMovementComponent : public UBaseMovementComponent
{
	GENERATED_BODY()

public:	
	UBossMovementComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadOnly, Category = "BossMovement")
	ABossCharacter * BossCharacterReference = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "BossMovement")
	float ChargeSpeed = 800.f;

	UPROPERTY(EditDefaultsOnly, Category = "BossMovement")
	float JumpInterpSpeed = 1.8f;

	UPROPERTY(EditDefaultsOnly, Category = "BossMovement")
	float MinChaseDuration = 2.f;

	UPROPERTY(EditDefaultsOnly, Category = "BossMovement")
	float MaxChaseDuration = 5.f;

	UFUNCTION(BlueprintCallable, Category = "Movement")
	void SwitchToChaseSpeed();

	UFUNCTION()
	void SwitchToStandardSpeed();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	void MoveToJumpLocation();
};
