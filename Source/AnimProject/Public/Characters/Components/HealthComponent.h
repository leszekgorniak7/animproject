// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLowHealth);

class ABaseCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ANIMPROJECT_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UHealthComponent();

	UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnDeath OnDeath;

	UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnDeath OnLowHealth;

	UPROPERTY(EditDefaultsOnly, Category = "Health")
	float MaxHealth = 100;

	/*Set to false, if you want the character to keep attacking when taking damage*/
	UPROPERTY(EditDefaultsOnly, Category = "Health")
	bool bDamageInterruptsAttack = true;

	/*Fires OnLowHealth event when health percentage falls below it*/
	UPROPERTY(EditDefaultsOnly, Category = "Health")
	float LowHealthTreshold = 0.5f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")
	bool bIsAlive = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	UAnimMontage * HitAnimation = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	UAnimMontage * DeathAnimation = nullptr;

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetCurrentHealth() const;

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetHealthPercentage() const;

	UFUNCTION(BlueprintPure, Category = "Health")
	bool HasLowHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Health")
	void AddHealth(float Amount);

	UFUNCTION()
	void HandleDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	void SetInvincible(bool IsInvincible);

	bool bIsTakingDamage = false;

protected:
	virtual void BeginPlay() override;

private:
	float CurrentHealth;
	bool bIsInvincible = false;

	void MakeDead();

	ABaseCharacter * CharacterOwner = nullptr;
	UAnimInstance * CharacterAnimInstance = nullptr;
		
};
