// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BaseAIController.generated.h"

class ABaseCharacter;
/**
 * 
 */
UCLASS()
class ANIMPROJECT_API ABaseAIController : public AAIController
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, Category = "AI")
	AActor* Target = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FName ThrowableObjectKey = "ThrowableObject";

	UPROPERTY(EditAnywhere, Category = "AI")
	float ThrowableObjectSearchRadius = 10000.f;

	UFUNCTION(BlueprintPure, Category = "AI")
	virtual bool CheckMelee(AActor* EnemyTarget) const;

	/**
	 * @return				true if ThrowableObjectPickup was found, false otherwise
	 */
	UFUNCTION(BlueprintCallable, Category = "AI")
	bool FindThrowableObject();

protected:
	virtual void OnPossess(APawn* InPawn) override;
	ABaseCharacter* PossessedCharacter = nullptr;
	
};
