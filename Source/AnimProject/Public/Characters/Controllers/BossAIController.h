// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Characters/Controllers/BaseAIController.h"
#include "BossAIController.generated.h"

class ABossCharacter;

UCLASS()
class ANIMPROJECT_API ABossAIController : public ABaseAIController
{
	GENERATED_BODY()
public:
	virtual bool CheckMelee(AActor* EnemyTarget) const override;

	UPROPERTY(BlueprintReadOnly, Category = "Boss AI")
	ABossCharacter* BossReference = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boss AI")
	FName UsingAbilityKey = "UsingAbility";

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Boss AI")
	float LongDistance = 700.f;

	UPROPERTY(BlueprintReadWrite, Category = "Boss AI")
	float ChaseTimer;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Boss AI")
	float ChaseTime = 8.f;

	UPROPERTY(BlueprintReadWrite, Category = "Boss AI")
	float RetreatTimer;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Boss AI")
	float RetreatTime = 3.f;

	UPROPERTY(BlueprintReadWrite, Category = "Boss AI")
	bool bIsChasing = false;

	UPROPERTY(BlueprintReadWrite, Category = "Boss AI")
	bool bCircleRight = false;

	UFUNCTION(BlueprintCallable, Category = "Boss AI")
	void OnAbilityFinished();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Boss AI")
	void RunSituationCheck();

protected:
	virtual void OnPossess(APawn* InPawn) override;
};
