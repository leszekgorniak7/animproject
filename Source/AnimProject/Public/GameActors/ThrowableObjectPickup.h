// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/Throwable.h"
#include "ThrowableObjectPickup.generated.h"

UCLASS()
class ANIMPROJECT_API AThrowableObjectPickup : public AActor, public IThrowable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AThrowableObjectPickup();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Throwable")
	void OnPickedUp();
	virtual void OnPickedUp_Implementation() override;

};
