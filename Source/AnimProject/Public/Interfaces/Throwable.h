// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Throwable.generated.h"

UINTERFACE(MinimalAPI)
class UThrowable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ANIMPROJECT_API IThrowable
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Throwable")
	void OnPickedUp();
};
