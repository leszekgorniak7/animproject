// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"
#include "MyPlayerInputComponent.generated.h"

class AMyPlayerCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ANIMPROJECT_API UMyPlayerInputComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UMyPlayerInputComponent();

	void InitializeInput();

	void LookUp(float Value);
	void LookUpRate(float Value);
	void Turn(float Value);
	void TurnRate(float Value);
	void MoveForward(float Value);
	void MoveRight(float Value);
	void Jump();
	void StopJumping();
	void LockOnTarget();
	void Dodge();
	void FastAttack();
	void StrongAttack();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UInputComponent *InputComponent = nullptr;
	AMyPlayerCharacter * PlayerCharacter = nullptr;

	UPROPERTY(EditAnywhere)
	float BaseTurnRate = 45.f;
	UPROPERTY(EditAnywhere)
	float BaseLookUpRate = 45.f;
		
};
