// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseCharacter.h"
#include "Engine/Classes/GameFramework/CharacterMovementComponent.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Engine/Classes/Components/PrimitiveComponent.h"
#include "MyPlayerCharacter.generated.h"

UENUM(BlueprintType)
enum class EAttackType : uint8
{
	Fast UMETA(DisplayName = "Fast"),
	Strong UMETA(DisplayName = "Strong")
};

class UMyPlayerInputComponent;
class UPlayerHUD;

UCLASS()
class ANIMPROJECT_API AMyPlayerCharacter : public ABaseCharacter
{
	GENERATED_BODY()

public:
	AMyPlayerCharacter();

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Player Animations")
	UAnimMontage * DodgeAnimation = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Player Animations")
	UAnimMontage * FastAttack_01_Animation = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Player Animations")
	UAnimMontage * FastAttack_02_Animation = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Player Animations")
	UAnimMontage * StrongAttack_01_Animation = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Player Animations")
	UAnimMontage * StrongAttack_02_Animation = nullptr;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player Character")
	bool bIsDodging = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player Character")
	bool bLockedOnTarget = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player Character")
	UPlayerHUD * PlayerHUD = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player Character")
	UCameraComponent * FollowCameraComp = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "Player Character")
	EAttackType AttackType;

	void ToggleLockOnTarget();
	void TryLockOnTarget();
	
	UFUNCTION(BlueprintCallable, Category = "Player Character")
	void DisableLockOnTarget();

	UFUNCTION(BlueprintCallable, Category = "Player Character")
	void DisableLockOnIfTargetKilled(APawn* KilledPawn);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Player Character")
	void OnBossDefeated(APawn* Boss);

	void FollowLockedTarget();
	void OnDodgeInvincibilityEnd();
	void OnDodgeEnd();
	void OnDodgeInvincibilityStart();
	virtual void ResetCombo() override;
	virtual void OnDamageAnim(bool bStarted) override;
	void Dodge();
	void FastAttack();
	void StrongAttack();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Character")
	APawn * FocusPawn = nullptr;

protected:
	virtual void BeginPlay() override;

private:
	float DodgeTimer = 0.f;
	int32 FastAttackCount = 0;
	int32 StrongAttackCount = 0;
	
	UCharacterMovementComponent * CharacterMovementComponent = nullptr;
	UMyPlayerInputComponent * MyPlayerInputComponent = nullptr;
	APlayerController * PlayerController = nullptr;

	UPrimitiveComponent * FocusPoint = nullptr;
};
