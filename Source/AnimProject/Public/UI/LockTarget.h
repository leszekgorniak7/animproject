// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LockTarget.generated.h"

/**
 * 
 */
UCLASS()
class ANIMPROJECT_API ULockTarget : public UUserWidget
{
	GENERATED_BODY()
	
};
