// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerHUD.generated.h"

class ULockTarget;

/**
 * 
 */
UCLASS()
class ANIMPROJECT_API UPlayerHUD : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetLockMarkerPosition(FVector TargetPosition);
	void HideLockMarker();

	UPROPERTY(BlueprintReadWrite, Category = "HUD")
	ULockTarget* LockTargetWidget = nullptr;
	
};
