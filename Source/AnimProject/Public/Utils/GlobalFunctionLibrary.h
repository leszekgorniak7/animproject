// Copyright by LG7

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GlobalFunctionLibrary.generated.h"

UCLASS()
class ANIMPROJECT_API UGlobalFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/*Returns an actor from the Actors array that is nearest to the ReferenceActor*/
	UFUNCTION(BlueprintPure, Category = "Global Functions")
	static AActor* FindNearestActor(AActor* ReferenceActor, TArray<AActor*> Actors);

	/*Simulates simple dice throw*/
	UFUNCTION(BlueprintCallable, Category = "Global Functions")
	static int32 ThrowDice(int32 Sides);
	
};
